create database db_HRM
go

use db_HRM 
go

create table TblDepartment(
Id int primary key identity(1,1),
DepartmentName varchar(50) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblDesignation(
Id int primary key identity(1,1),
DesignationName varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)
use db_HRM
select * from TblAssignedLoan

create table TblHoliday(
Id int primary key identity(1,1),
Name varchar(50) not null,
StartDate datetime not null,
EndDate datetime not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

insert into TblEmployee values
('Abdullah','Muafa', 'M132800', 12345678, 1, 1,1,'L',('2000/08/31'),getdate(),getdate(),'Deny',2,1,getdate(),null, null,0,null,null)

create table TblEmployee(
Id int primary key identity(1,1) not null,
FirstName varchar(50) not null,
LastName varchar(50) not null,
PIN varchar(50) not null,
NID bigint not null,
DepartmentID int not null,
DesignationID int not null,
BloodGroupID int not null,
Gender varchar(10) not null,
DOB datetime not null,
JoinDate datetime not null,
DateOfLeaving datetime null,
[Status] varchar(50) not null,
EarnedLeave bigint null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblAssignedTask(
Id int primary key identity(1,1),
TaskID int not null,
EmployeeID int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblDisciplinary(
Id int primary key identity(1,1),
EmployeeID int not null,
Title varchar(50) not null,
Description varchar(100) not null,
Status varchar(50) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblLeaveType(
Id int primary key identity(1,1),
Type varchar(50) not null,
NumberOfDays varchar(50) null,
Status varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblLeave(
Id int primary key identity(1,1),
EmployeeId int not null,
LeaveTypeId int not null,
Duration int not null,
[Date] datetime not null, 
[Length] int not null,
Reason varchar(100),
Approval bit not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblBloodGroup(
Id int primary key identity(1,1),
BloodType varchar(20) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblSocialMedia(
Id int primary key identity(1,1),
EmployeeID int not null,
Facebook varchar(50) not null,
Twitter varchar(50) not null,
Google varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblTask(
Id int primary key identity(1,1),
ProjectId int not null,
TaskTitle varchar(50),
StartDate datetime not null,
EndDate datetime not null,
Summary varchar(50),
Details varchar(100),
[Status] varchar(50),
[Type] varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

--create table AssignedTask(
--Id int primary key identity(1,1),
--TaskID int not null,
--EmployeeId int not null,
--CreatedBy int not null,
--CreatedOn datetime not null,
--ModifiedBy int null,
--ModifiedOn datetime null,
--IsDelete bit not null,
--DeletedBy int null,
--DeletedOn datetime null,
--)

create table TblAddress(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
City varchar(50) not null,
FullAddress varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblEducation(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
DegreeTitle varchar(100) not null,
InstituteName varchar(100) not null,
Result varchar(50),
PassingYear varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblExperience(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
CompanyName varchar(100) not null,
Position varchar(50) not null,
[Address] varchar(100) not null,
WorkingDuration varchar(50) null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblBankAccount(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
BankName varchar(50) not null,
BranchName varchar(100) not null,
AccountNumber varchar(50) not null,
AccountType varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblDocument(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
Title varchar(50) not null,
[File] varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblSalary(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
SalaryType varchar(50) not null,
[Basic] varchar(50) not null,
Insurance varchar(50) not null,
Tax int not null,
ProvidentFund int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblProject(
Id int primary key identity(1,1) not null,
ProjectTitle varchar(100) not null,
StartDate datetime not null,
EndDate datetime not null,
Summary varchar(50) not null,
Details varchar(100) not null,
Status varchar(50) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblAttendance(
Id int primary key identity(1,1) not null,
EmployeeID int not null,
AttendanceDate datetime not null,
SignInTime time not null,
SignOutTime time not null,
Place varchar(100),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblPayroll(
Id int primary key identity(1,1) not null,
SalaryID int not null,
WorkedHours datetime not null,
[Month] datetime not null,
PayDate datetime not null,
PayType varchar(50) not null,
[Status] varchar(100),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblAssetCategory(
ID int primary key identity(1,1) not null,
[Type] varchar(50),
Name varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblInstallment(
ID int primary key identity(1,1) not null,
[Date] datetime not null,
Receiver varchar(50),
Notes varchar(100),
InstallNumber varchar(50),
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblLoan(
ID int primary key Identity(1,1),
LoanNumber nvarchar(20) not null,
Amount bigint not null,
InstallPeriod bigint,
InstallAmount decimal(18,0),
[Status] varchar(50),
ApprovalDate datetime,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)
select * from tblloan
insert into TblLoan values(
	'23042501',100000 , 10 , 10000, 'Granted', getdate(), 1, getdate(), null, null, 0, null, null
)

select concat(FirstName, ' ', LastName) as Name, EmployeeID as Employee_Code, Amount, (Amount / l.InstallPeriod) as installment, l.LoanNumber, l.[Status]
from TblAssignedLoan as a
                    join TblLoan as l on a.LoanNumberID = l.ID
                    join TblEmployee as e on a.EmployeeId = e.Id

insert into TblAssignedLoan 
VALUES(1,1,1,1, getdate(), null, null, 0, null, null)

select * from TblLoan
select * from TblAssignedLoan

create table TblAssignedLoan(
ID int primary key identity(1,1) not null,
LoanNumberID bigint not null,
EmployeeID int not null,
InstallmentID int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblAsset(
ID int primary key identity(1,1) not null,
AssetCategoryID int not null,
Name varchar(50) not null,
Brand varchar(50) not null,
Model varchar(50) not null,
Code varchar(50) not null,
Configuration varchar(100) null,
PurchasingDate datetime not null,
Price int not null,
Quantity varchar(100) null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblLogisticSupport(
ID int primary key identity(1,1) not null,
AssetID int not null,
AssignedTaskID int not null,
StartDate datetime not null,
EndDate datetime not null,
BackDate datetime not null,
AssignQty int not null,
ReturnQty int not null,
Remarks varchar(100) null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblAccount(
ID int primary key identity(1,1) not null,
EmployeeID int not null,
Username varchar(100) not null,
[Password] varchar(50) not null,
ProfilePicture varchar(max),
RoleID int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblRole(
ID int primary key identity(1,1) not null,
RoleName varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblContactPerson(
Id int primary key identity(1,1),
EmployeeID int not null,
Email varchar(100) not null,
PhoneNumber int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblNotice(
Id int primary key identity(1,1) not null,
Title varchar(100) not null,
[File] varchar(max) not null,
PublishDate datetime,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblSalaryBenefit(
Id int primary key identity(1,1) not null,
SalaryID int not null,
Benefit int not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblBenefit(
Id int primary key identity(1,1) not null,
Medical decimal(18,0) not null,
HouseRent decimal(18,0) not null,
Conveyance decimal(18,0) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

create table TblFieldVisit(
Id int primary key identity(1,1) not null,
ProjectID int not null,
EmployeeID int not null,
FieldLocation varchar(max) not null,
ApproxStartDate datetime not null,
ApproxEndData datetime not null,
ActualEndDate datetime not null,
[Status] varchar(100) not null,
Notes varchar(100) not null,
CreatedBy int not null,
CreatedOn datetime not null,
ModifiedBy int null,
ModifiedOn datetime null,
IsDelete bit not null,
DeletedBy int null,
DeletedOn datetime null,
)

select * from TblDesignation

select * from TblDepartment
insert into TblDepartment values(
	'Research',1,getdate(),0,getdate(),0,0,getdate()
)