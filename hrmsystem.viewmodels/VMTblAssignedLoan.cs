﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrmsystem.viewmodels
{
    public class VMTblAssignedLoan
    {
        public int? Id { get; set; }

        public long? LoanNumberId { get; set; }
        public string? LoanNumber { get; set; }
        public long? Amount { get; set; }
        public long? InstallPeriod { get; set; }
        public decimal? InstallAmount { get; set; }
        public decimal? Installment { get; set; }
        public string? Status { get; set; }
        public DateTime? ApprovalDate { get; set; }

        public int EmployeeId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        //public string? FullName { get { return this.FirstName + " " + this.LastName; } }

        public string? FullName { get; set; }

        //public int? InstallmentId { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool IsDelete { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
