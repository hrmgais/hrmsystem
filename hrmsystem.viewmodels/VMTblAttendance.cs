﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrmsystem.viewmodels
{
	public class VMTblAttendance
	{
		public int Id { get; set; }
		
		public int EmployeeId { get; set; }
		
		public DateTime AttendanceDate { get; set; }
		public DateTime SignInTime { get; set; }
		public DateTime SignOutTime { get; set; }
		
		public string? Place { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int? ModifiedBy { get; set; }
		public DateTime? ModifiedOn { get; set; }
		public bool IsDelete { get; set; }
		public int? DeletedBy { get; set; }
		public DateTime? DeletedOn { get; set; }

		public TimeSpan? WorkingHour { get; set; }
		public string? FirstName { get; set; }

		public string? LastName { get; set; }
		public string Pin { get; set; } = null!;
		public string? Fullname { get { return this.FirstName + " " + this.LastName; } }

	}
}
