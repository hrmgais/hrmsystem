﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hrmsystem.viewmodels
{
	public class VMTblEmployee
	{
		public int Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }
        public string FullName { get; set; }
        //public string FullName { get { return this.FirstName + " " + this.LastName; } }
        public string Pin { get; set; } = null!;

		public long Nid { get; set; }

		public int DepartmentId { get; set; }

		public int DesignationId { get; set; }
		public int BloodGroupId { get; set; }
		public string Gender { get; set; } = null!;

		public DateTime Dob { get; set; }

		public string Email { get; set; } = null!;
		public DateTime JoinDate { get; set; }
		public DateTime? DateOfLeaving { get; set; }
		public string Status { get; set; } = null!;
		public long? EarnedLeave { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public int? ModifiedBy { get; set; }
		public DateTime? ModifiedOn { get; set; }
		public bool IsDelete { get; set; }
		public int? DeletedBy { get; set; }
		public DateTime? DeletedOn { get; set; }
		public string? Fullname { get; set; }
	}
}