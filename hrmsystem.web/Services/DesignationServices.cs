﻿using Azure;
using hrmsystem.datamodels;
using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace hrmsystem.web.Services
{
    public class DesignationServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public DesignationServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblDesignation>> GetAllData()
        {
            List<TblDesignation> data = new List<TblDesignation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDesignation/GetAllData");

            data = JsonConvert.DeserializeObject<List<TblDesignation>>(apiResponse)!;

            return data;
        }

        public async Task<bool> CheckDesignationByName(string nameDesignation)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiDesignation/CheckDesignationByName/{nameDesignation}");

            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

            return isExist;
        }

        public async Task<VMResponse> Create(TblDesignation dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PostAsync(RouteAPI + "apiDesignation/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<TblDesignation> GetDataById(int id)
        {
            TblDesignation data = new TblDesignation();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiDesignation/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblDesignation>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblDesignation dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PutAsync(RouteAPI + "apiDesignation/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiDesignation/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;

        }
    }
}
