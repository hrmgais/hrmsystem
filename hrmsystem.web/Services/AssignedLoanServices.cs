﻿using hrmsystem.viewmodels;
using Newtonsoft.Json;

namespace hrmsystem.web.Services
{
    public class AssignedLoanServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        //cara dapetin value dari appsetting json
        public AssignedLoanServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblAssignedLoan>> GetAllData()
        {
            //list data
            List<VMTblAssignedLoan> data = new List<VMTblAssignedLoan>();

            string apiResponse = await client.GetStringAsync(RouteAPI+"apiAssignedLoan/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMTblAssignedLoan>>(apiResponse)!;

            return data;
        }
    }
}
