﻿using System.Text;
using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Newtonsoft.Json;

namespace hrmsystem.web.Services
{
    public class LoanServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public LoanServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblLoan>> GetAllData()
        {
            List<TblLoan> data = new List<TblLoan>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiLoan/GetAllData");

            data = JsonConvert.DeserializeObject<List<TblLoan>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create( TblLoan dataParam)
        {
            //Proses convert dari obj to string
            string json = JsonConvert.SerializeObject(dataParam);


            //Proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //Proses memangil API dan mengirim data body
            var request = await client.PostAsync(RouteAPI + "apiLoan/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //Proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
