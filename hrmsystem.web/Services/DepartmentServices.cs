﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Newtonsoft.Json;
using System.Drawing;
using System.Text;

namespace hrmsystem.web.Services
{
    public class DepartmentServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public DepartmentServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblDepartment>> GetAllData()
        {
            List<TblDepartment> data = new List<TblDepartment>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDepartment/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblDepartment>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(TblDepartment dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            // proses mengubah string menjadi json, lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiDepartment/Save", content);

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon dari API
                var apiResponse = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<TblDepartment> GetDataById(int id)
        {
            TblDepartment data = new TblDepartment();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiDepartment/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblDepartment>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblDepartment dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            // proses mengubah string menjadi json, lalu dikirim sebagai Request Body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirim Body
            var request = await client.PutAsync(RouteAPI + "apiDepartment/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon dari API
                var apiResponse = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiDepartment/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon dari API
                var apiResponse = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
