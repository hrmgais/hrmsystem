﻿using System.Text;
using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Newtonsoft.Json;

namespace hrmsystem.web.Services
{
	public class AttendanceServices
	{
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse respon = new VMResponse();

		public AttendanceServices(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}
		public async Task<List<VMTblAttendance>> GetAllData()
		{
			List<VMTblAttendance> data = new List<VMTblAttendance>();
			string apiRespone = await client.GetStringAsync(RouteAPI + "apiAttendance/GetAllData");
			data = JsonConvert.DeserializeObject<List<VMTblAttendance>>(apiRespone)!;
			return data;
		}

		public async Task<VMTblAttendance> GetDataById(int id)
		{
			VMTblAttendance data = new VMTblAttendance();
			string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAttendance/GetDataById/{id}");
			data = JsonConvert.DeserializeObject<VMTblAttendance>(apiResponse)!;
			return data;
		}

		public async Task<VMResponse> AddAttendance(VMTblAttendance dataParam)
		{
			
			string json = JsonConvert.SerializeObject(dataParam);
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			var request = await client.PostAsync(RouteAPI + "apiAttendance/Save", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return respon;
		}

		public async Task<VMResponse> EditAttendance(VMTblAttendance dataParam)
		{
			string json = JsonConvert.SerializeObject(dataParam);
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
			var request = await client.PutAsync(RouteAPI + "apiAttendance/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiRespon = await request.Content.ReadAsStringAsync();
				//proses convert hasil respon dari API ke object
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode}: {request.ReasonPhrase}";
			}
			return respon;
		}
	}
}
