﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Newtonsoft.Json;

namespace hrmsystem.web.Services
{
	public class EmployeeServices
	{
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteAPI = "";
		private VMResponse respon = new VMResponse();

		public EmployeeServices(IConfiguration _configuration)
		{
			this.configuration = _configuration;
			this.RouteAPI = this.configuration["RouteAPI"];
		}
		public async Task<List<VMTblEmployee>> GetAllData()
		{
			List<VMTblEmployee> data = new List<VMTblEmployee>();
			string apiRespone = await client.GetStringAsync(RouteAPI + "apiEmployee/GetAllData");
			data = JsonConvert.DeserializeObject<List<VMTblEmployee>>(apiRespone)!;
			return data;
		}
	}
}