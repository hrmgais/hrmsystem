﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using hrmsystem.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.web.Controllers
{
    public class AttendanceController : Controller
    {
        public EmployeeServices employeeServices;
        public AttendanceServices attendanceServices;

        public AttendanceController(EmployeeServices _employeeServices, AttendanceServices _attendanceServices)
        {
            this.employeeServices = _employeeServices;
            this.attendanceServices = _attendanceServices;
        }

        public async Task<IActionResult> AttendanceList()
        {
            List<VMTblAttendance> data = await attendanceServices.GetAllData();
            return View(data);
        }

        public async Task<IActionResult> AddAttendance()
        {
            VMTblAttendance data = new VMTblAttendance();
            List<VMTblEmployees> listEmployee = await employeeServices.GetAllData();
            ViewBag.ListEmployee = listEmployee;
            ViewBag.ListPlace = new List<VMDropDown>()
            {
                new VMDropDown{Value = "Office", Name = "Office" },
                new VMDropDown{Value = "Field", Name = "Field" }
            };
            return View(data);
        }
		[HttpPost]
		public async Task<IActionResult> AddAttendance(VMTblAttendance dataParam)
		{
			VMResponse respon = await attendanceServices.AddAttendance(dataParam);
			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

        public async Task<IActionResult> EditAttendance(int id)
        {
            VMTblAttendance data = await attendanceServices.GetDataById(id);
			List<VMTblEmployees> listEmployee = await employeeServices.GetAllData();
			ViewBag.ListEmployee = listEmployee;
			ViewBag.ListPlace = new List<VMDropDown>()
			{
				new VMDropDown{Value = "Office", Name = "Office" },
				new VMDropDown{Value = "Field", Name = "Field" }
			};
			return PartialView(data);
        }

		public async Task<IActionResult> AttendanceReport()
        {
            return View();
        }

        public async Task<IActionResult> AddBulkAttendance()
        {
            return View();
        }
    }
}
