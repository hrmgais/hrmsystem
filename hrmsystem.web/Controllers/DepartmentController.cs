﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using hrmsystem.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.web.Controllers
{
    public class DepartmentController : Controller
    {
        private DepartmentServices departmentServices;

        public DepartmentController(DepartmentServices _departmentServices)
        {
            this.departmentServices = _departmentServices;
        }

        public async Task<IActionResult> Index()
        {
            List<TblDepartment> data = await departmentServices.GetAllData();
            return View(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblDepartment dataParam)
        {
            VMResponse respon = await departmentServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblDepartment data = await departmentServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblDepartment dataParam)
        {
            VMResponse respon = await departmentServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblDepartment data = await departmentServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await departmentServices.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index", id);
        }
    }
}
