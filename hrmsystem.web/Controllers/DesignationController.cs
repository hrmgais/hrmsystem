﻿using Azure;
using hrmsystem.datamodels;
using hrmsystem.web.Services;
using Microsoft.AspNetCore.Mvc;
using xpos314.viewmodels;

namespace hrmsystem.web.Controllers
{
    public class DesignationController : Controller
    {
        private readonly DesignationServices designationServices;
        public int IdUser = 1;

        public DesignationController(DesignationServices _designationServices)
        {
            this.designationServices = _designationServices;
        }

        public async Task<IActionResult> Index()
        {
            List<TblDesignation> data = await designationServices.GetAllData();

            return View(data);
        }

        public async Task<JsonResult> CheckNameIsExist(string nameDesignation)
        {
            bool isExist = await designationServices.CheckDesignationByName(nameDesignation);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblDesignation dataParam)
        {
            VMResponse respon = await designationServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblDesignation data = await designationServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblDesignation dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            VMResponse respon = await designationServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await designationServices.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            else
            {
                return RedirectToAction("Index", id);
            }
        }


    }
}
