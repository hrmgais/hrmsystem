﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using hrmsystem.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.web.Controllers
{
    public class AssignedLoanController : Controller
    {
        private LoanServices loanServices;
        private EmployeeServices employeeServices;
        private AssignedLoanServices assignedLoanServices;

        public AssignedLoanController(LoanServices _loanServices, EmployeeServices _employeeServices, AssignedLoanServices _assignedLoanServices)
        {
            this.loanServices = _loanServices;
            this.employeeServices = _employeeServices;
            this.assignedLoanServices = _assignedLoanServices;
        }
        public async Task<IActionResult> Index()
        {
            List<VMTblAssignedLoan> data = await assignedLoanServices.GetAllData();
            
            return View(data);
        }

        //public async Task<IActionResult> Create()
        //{
        //    VMTblAssignedLoan data = new VMTblAssignedLoan();

        //    List<TblLoan> listLoan = await loanServices.GetAllData();
        //    ViewBag.ListLoan= listLoan;
        //    List<VMTblEmployee> listEmployee = await employeeServices.GetAllData();
        //    ViewBag.ListEmployee = listEmployee;

        //    return PartialView(data);
        //}

        //[HttpPost]
        //public async Task<IActionResult> Create(TblLoan dataParam)
        //{
        //    VMResponse respon = await loanServices.Create(dataParam);

        //    if (respon.Success)
        //    {
        //        return Json(new { dataRespon = respon });
        //    }

        //    return View(dataParam);
        //}

    }
}
