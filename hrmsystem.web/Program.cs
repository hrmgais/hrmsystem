using Microsoft.EntityFrameworkCore;
using hrmsystem.web.Services;
using hrmsystem.datamodels;
using hrmsystem.viewmodels;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddScoped<DesignationServices>();
builder.Services.AddScoped<DepartmentServices>();
builder.Services.AddScoped<EmployeeServices>();
builder.Services.AddScoped<AssignedLoanServices>();
builder.Services.AddScoped<LoanServices>();

//Add Session
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSession(option =>
{
    option.IdleTimeout = TimeSpan.FromHours(1);
    option.Cookie.HttpOnly = true;
    option.Cookie.IsEssential = true;
});

// Add connection String
builder.Services.AddDbContext<db_HRMContext>(option =>
{
    option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

//Add Session
app.UseSession();

app.MapControllerRoute(
    name: "default",
    //pattern: "{controller=Auth}/{action=Login}/{id?}");
    pattern: "{controller=Home}/{action=Index}");

app.Run();
