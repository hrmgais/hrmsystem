﻿using Azure;
using hrmsystem.datamodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.viewmodels;

namespace hrmsystem.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiDesignationController : ControllerBase
    {
        private readonly db_HRMContext db;
        private readonly VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiDesignationController(db_HRMContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblDesignation> GetAllData()
        {
            List<TblDesignation> data = db.TblDesignations.Where(a=>a.IsDelete==false).ToList();
            return data;
        }

        [HttpGet("CheckDesignationByName/{name}")]
        public bool CheckName(string name)
        {
            TblDesignation data = db.TblDesignations.Where(a => a.DesignationName == name).FirstOrDefault()!;
            if (data != null)
            {
                return true;
            }
            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblDesignation data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved " + ex.Message;
            }
            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public TblDesignation DataById(int id)
        {
            TblDesignation result = new TblDesignation();

            result = db.TblDesignations.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblDesignation data)
        {
            TblDesignation dt = db.TblDesignations.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DesignationName = data.DesignationName;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    respon.Message = "Data Success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Saved Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblDesignation dt = db.TblDesignations.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = $" Data {dt.DesignationName} Success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

    }
}
