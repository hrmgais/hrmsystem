﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiEmployeeController : ControllerBase
    {
        private readonly db_HRMContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiEmployeeController(db_HRMContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblEmployee> GetAllData()
        {
            List<TblEmployee> data = db.TblEmployees.Where(a => a.IsDelete == false).ToList();
            return data;
        }
    }
}
