﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using hrmsystem.datamodels;
using hrmsystem.viewmodels;

namespace hrmsystem.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiDepartmentController : ControllerBase
    {
        private readonly db_HRMContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiDepartmentController(db_HRMContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblDepartment> GetAllData()
        {
            List<TblDepartment> data = db.TblDepartments.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblDepartment DataById(int id)
        {
            TblDepartment result = new TblDepartment();
            result = db.TblDepartments.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblDepartment data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception err)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + err.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblDepartment data)
        {
            TblDepartment dt = db.TblDepartments.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DepartmentName = data.DepartmentName;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success updated";
                }
                catch (Exception err)
                {
                    respon.Success = false;
                    respon.Message = "Update failed : " + err.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblDepartment dt = db.TblDepartments.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = $"Data {dt.DepartmentName} success deleted";
                }
                catch (Exception err)
                {
                    respon.Success = false;
                    respon.Message = "Delete failed : " + err.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }
    }
}
