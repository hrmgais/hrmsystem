﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiAttendanceController : ControllerBase
	{
		private readonly db_HRMContext db;
		private VMResponse respon = new VMResponse();
		private int IdUser = 1;

		public apiAttendanceController(db_HRMContext _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<VMTblAttendance> GetAllData()
		{
			List<VMTblAttendance> data = (from a in db.TblAttendances
										  join e in db.TblEmployees on a.EmployeeId equals e.Id
										  where a.IsDelete == false
										  select new VMTblAttendance
										  {
											  Id = a.Id,
											  EmployeeId = a.EmployeeId,
											  AttendanceDate = a.AttendanceDate,
											  SignInTime = a.SignInTime,
											  SignOutTime = a.SignOutTime,
											  Place = a.Place,
											  CreatedBy = a.CreatedBy,
											  CreatedOn = a.CreatedOn,
											  ModifiedBy = a.ModifiedBy,
											  ModifiedOn = a.ModifiedOn,
											  DeletedBy = a.DeletedBy,
											  DeletedOn = a.DeletedOn,
											  WorkingHour = a.SignOutTime.Subtract(a.SignInTime),

											  FirstName = e.FirstName,
											  LastName = e.LastName,
											  Pin = e.Pin
											  
										  }).ToList();


			return data;
		}

		[HttpGet("GetDataById/{id}")]
		public VMTblAttendance GetDataById(int id)
		{
			VMTblAttendance data = (from a in db.TblAttendances
									join e in db.TblEmployees on a.EmployeeId equals e.Id
									where a.IsDelete == false && a.Id == id
									select new VMTblAttendance
									{
										Id = a.Id,
										EmployeeId = a.EmployeeId,
										AttendanceDate = a.AttendanceDate,
										SignInTime = a.SignInTime,
										SignOutTime = a.SignOutTime,
										Place = a.Place,
										CreatedBy = a.CreatedBy,
										CreatedOn = a.CreatedOn,
										ModifiedBy = a.ModifiedBy,
										ModifiedOn = a.ModifiedOn,
										DeletedBy = a.DeletedBy,
										DeletedOn = a.DeletedOn,
										WorkingHour = a.SignOutTime.Subtract(a.SignInTime),

										FirstName = e.FirstName,
										LastName = e.LastName,
										Pin = e.Pin

									}).FirstOrDefault()!;

			return data;
		}

		[HttpPost("Save")]
		public VMResponse Save(TblAttendance data)
		{
			data.CreatedBy = IdUser;
			data.CreatedOn = DateTime.Now;
			data.IsDelete = false;
			//data.AttendanceDate = DateTime.Now;
			try
			{
				db.Add(data);
				db.SaveChanges();

				respon.Message = "Data success saved";
			}
			catch (Exception e)
			{
				respon.Success = false;
				respon.Message = "Failed saved " + e.Message;
			}
			return respon;
		}

		[HttpPut("Edit")]
		public VMResponse Edit(TblAttendance data)
		{
			TblAttendance dt = db.TblAttendances.Where(a => a.Id ==  data.Id).FirstOrDefault()!;

			if (dt != null)
			{
				dt.EmployeeId = data.EmployeeId;
				dt.ModifiedOn = DateTime.Now;
				dt.ModifiedBy = IdUser;
				dt.AttendanceDate = data.AttendanceDate;
				dt.SignInTime = data.SignInTime;
				dt.SignOutTime = data.SignOutTime;
				dt.Place = data.Place;
				//data.AttendanceDate = DateTime.Now;
				try
				{
					db.Update(dt);
					db.SaveChanges();

					respon.Message = "Data success saved";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Failed saved " + e.Message;
				}
			}
			else
			{
				respon.Success = false;
				respon.Message = "Data not found";
			}		
			
			return respon;
		}
	}
}
