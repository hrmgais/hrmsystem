﻿using hrmsystem.datamodels;
using hrmsystem.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hrmsystem.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAssignedLoanController : ControllerBase
    {
        private readonly db_HRMContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiAssignedLoanController(db_HRMContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblAssignedLoan> GetAllData()
        {
            List<VMTblAssignedLoan> data = (
                from a in db.TblAssignedLoans
                join l in db.TblLoans on a.LoanNumberId equals l.Id
                join e in db.TblEmployees on a.EmployeeId equals e.Id
                where a.IsDelete == false
                select new VMTblAssignedLoan
                {
                    Id = a.Id,
                    EmployeeId = a.EmployeeId,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    FullName = e.FirstName + " " + e.LastName,
                    LoanNumberId = a.LoanNumberId,
                    LoanNumber = l.LoanNumber,
                    Amount = l.Amount,
                    InstallPeriod = l.InstallPeriod,
                    InstallAmount = l.InstallAmount,
                    Installment = l.Amount / l.InstallPeriod,
                    Status = l.Status,
                    ApprovalDate = l.ApprovalDate,
                    IsDelete = a.IsDelete,
                    CreatedOn = a.CreatedOn,
                }
                ).ToList();

            return data;
        }
    }
}
